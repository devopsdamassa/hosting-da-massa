# Hosting da massa 
Plataforma de hospedagem de sites estáticos.

## Ferramentas
<!-- ToDo -->
<!-- colocar os links para cada ferramenta -->

 - ingress-nginx
 - make
 - local-registry
 - tilt 
 - k3s
 - helm
 - operator-sdk-helm
 - python
 - angular 
 - angular material
 - Jekyll

# Commandos

  -  curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION="v1.24.9-rc1+k3s2" INSTALL_K3S_EXEC=" --disable traefik --write-kubeconfig-mode 644" sh -s -
  - /usr/local/bin/k3s-uninstall.sh
  - make docker-build docker-push IMG="registry.devopsdamassa.com/deployer:0.0.0.1"

# Doc
 - https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html
 - https://sdk.operatorframework.io/docs/building-operators/helm/
 - https://jekyllrb.com/docs/
 - https://pet2cattle.com/2021/04/k3s-join-nodes
 - https://docs.tilt.dev/