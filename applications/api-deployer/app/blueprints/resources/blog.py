from flask import abort, jsonify
from flask_restful import Resource, reqparse
from kubernetes import client, config

config.load_incluster_config()
api = client.CustomObjectsApi()


class Blog(Resource):
  def post(self):
    parser = reqparse.RequestParser()
    parser.add_argument(
      'name',
      type=str,
      required=True,
      help='nome do cliente'
    )
    parser.add_argument(
      'namespace',
      type=str,
      required=True,
      help='namespace do cliente'
    )
    args = parser.parse_args()
    name = args['name']
    namespace = args['namespace']
    
    blog = {
      "apiVersion": "demo.devopsdamassa.com/v1alpha1",
      "kind": "Blog",
      "metadata": {
        "name": "",
        "namespace": ""
      },
      "spec": {
        "replicaCount": 5,
        "resource": {
          "limits": {
            "cpu": 1,
            "memory": "512Mi"
          }
        }
      }
    }
    blog["metadata"]["name"] = name
    blog["metadata"]["namespace"] = namespace
    
    api.create_namespaced_custom_object(
      group="demo.devopsdamassa.com",
      version="v1alpha1",
      namespace="hm-core",
      plural="blogs",
      body=blog,
    )
    
    return jsonify(
      {"msg":"deu bom"}
    )
  
  def get(self):
    parser = reqparse.RequestParser()
    parser.add_argument(
      'name',
      type=str,
      required=True,
      help='nome do cliente'
    )
    parser.add_argument(
      'namespace',
      type=str,
      required=True,
      help='namespace do cliente'
    )
    args = parser.parse_args()
    name = args['name']
    namespace = args['namespace']
    try:
      resource = api.get_namespaced_custom_object(
        group="demo.devopsdamassa.com",
        version="v1alpha1",
        namespace=namespace,
        name=name,
        plural="blogs",
      )
      return resource
    except:
      return jsonify({"msg": "deu ruim"})
