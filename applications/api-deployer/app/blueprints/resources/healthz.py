from flask import abort, jsonify, Response
from flask_restful import Resource
from kubernetes import client, config

config.load_incluster_config()
api = client.CoreApi()

class Check(Resource):
  def get(self):
    try:
      api_instance = api.get_api_versions()
      return Response("{'status':'200'}", status=200, mimetype='application/json')
    except:
      return Response("{'status':'500'}", status=500, mimetype='application/json')
