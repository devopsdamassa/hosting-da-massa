from flask import Blueprint
from flask_restful import Api

from .blog import Blog
from .healthz import Check

bd = Blueprint("blogs", "__name__", url_prefix="/")
api = Api(bd)

def init_app(app):
  api.add_resource(Blog, "/blog/")
  api.add_resource(Check, "/healthz")
  app.register_blueprint(bd)