## Daemon.json
No arquivo daemon.json vai algumas configs para o daemon do docker.

Para configurar o registry local, precisa copiar o arquivo para a pasta /etc/docker/, editar caso ele exista com as informações do arquivo [daemon.json](./daemon.json)

Depois precisa reiniciar o serviço do docker;

    sudo systemctl restart docker.service 

## Registries.yaml

Para configurar o registry local no k3s, precisa copiar o arquivo para a pasta /etc/rancher/k3s/, ou editar caso ele exista com as informações do arquivo [registries.yaml](registries.yaml)

Depois precisa reiniciar o serviço do k3s;

    sudo systemctl restart k3s.service

Vale lembrar de criar o apontamento do domain no /etc/hosts para o ip da maquina que está rodando o local-registry