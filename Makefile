
install_ingress_nginx:
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.7.1/deploy/static/provider/baremetal/deploy.yaml
	kubectl patch svc ingress-nginx-controller -n ingress-nginx -p '{"spec":{"externalTrafficPolicy": "Local","type": "LoadBalancer"}}'

install_local-registry:
	helm upgrade --install --namespace local-registry local-registry ./charts/local-registry --create-namespace

